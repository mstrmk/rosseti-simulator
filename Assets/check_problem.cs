using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oops : MonoBehaviour
{
    public GameObject panelGameOver;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            panelGameOver.SetActive(false);
        }
    }
}
