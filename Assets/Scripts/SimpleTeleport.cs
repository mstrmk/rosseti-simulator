using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTeleport : MonoBehaviour
{
    public Transform Target;
    private Rigidbody rb;
    private Quaternion startRotation;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        startRotation = transform.rotation;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
            Teleport();
    }

    public void Teleport()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = true;

        transform.position = Target.position;
        transform.rotation = startRotation;
        rb.isKinematic = false;

    }
}
