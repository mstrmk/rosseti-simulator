using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PointFinal : MonoBehaviour
{
    public int LevelToLoad = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            Debug.Log("End of level");
            SceneManager.LoadScene(LevelToLoad);
        }

    }
}
