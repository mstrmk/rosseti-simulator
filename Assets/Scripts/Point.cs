using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{

    [HideInInspector]
    public GameObject NextObject;


    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out Player player))
        {
            gameObject.SetActive(false);

            if(NextObject != null)
                NextObject.SetActive(true);
        }

    }
}
