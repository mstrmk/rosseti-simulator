using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointController : MonoBehaviour
{
    private List<GameObject> wayPoints = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            wayPoints.Add(transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < wayPoints.Count; i++)
        {
            if (wayPoints[i].TryGetComponent(out Point pt))
            {
                if (i < wayPoints.Count - 1)
                    pt.NextObject = wayPoints[i + 1];                       
            }
        }

        foreach (var point in wayPoints)
        {
            point.SetActive(false);
        }

        wayPoints[0].SetActive(true);
    }

}
