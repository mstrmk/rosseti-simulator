﻿using UnityEngine;
using System.Collections;

public class lookAt : MonoBehaviour {

    public Transform target;
    public GameObject pilot;
    public GameObject droneCam;

    void FixedUpdate() {
        float diffX = Mathf.Abs(Mathf.Abs(target.transform.position.x) - Mathf.Abs(transform.position.x));
        float diffY = Mathf.Abs(Mathf.Abs(target.transform.position.y) - Mathf.Abs(transform.position.y));
        float diffZ = Mathf.Abs(Mathf.Abs(target.transform.position.z) - Mathf.Abs(transform.position.z));
        //droneCam.active = false;
        if (diffX > 15f || diffY > 7.5f || diffZ > 15f) {
            pilot.active = false;
            droneCam.active = true;
        } else {
            droneCam.active = false;
            pilot.active = true;
            
        }

        
    }
}
